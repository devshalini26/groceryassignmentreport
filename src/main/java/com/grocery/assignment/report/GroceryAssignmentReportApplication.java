package com.grocery.assignment.report;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class GroceryAssignmentReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroceryAssignmentReportApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(){ return new RestTemplate();}
}
