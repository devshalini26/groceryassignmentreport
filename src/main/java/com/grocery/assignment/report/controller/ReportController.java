package com.grocery.assignment.report.controller;

import com.grocery.assignment.report.exception.ReportGenerationException;
import com.grocery.assignment.report.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Controller to generate report
 */
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/groceryReport")
public class ReportController {
    @Autowired
    private ReportService reportService;

    @GetMapping(path = "/getReport", params = {"itemName"})
    public ResponseEntity<byte[]> getGroceryReport(@RequestParam("itemName") String itemName) throws IOException {
        byte[] data = reportService.getGroceryReport(itemName);
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentLength(data.length);
        respHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        respHeaders.setContentDisposition(ContentDisposition.attachment().filename("itemReport.jpg").build());

        return new ResponseEntity<byte[]>(data, respHeaders, HttpStatus.OK);
    }

    @ExceptionHandler({ ReportGenerationException.class })
    public ResponseEntity<Object> handleException() {
        return new ResponseEntity<Object>("Error in generating report. Please try after sometime.", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
