package com.grocery.assignment.report.exception;

/**
 * Custom exception for issues in report generation
 */
public class ReportGenerationException extends RuntimeException {
    public ReportGenerationException(){}
    public ReportGenerationException(String message) { super(message); }
}
