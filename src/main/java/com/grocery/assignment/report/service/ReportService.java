package com.grocery.assignment.report.service;

import java.io.IOException;

/**
 * Service for Report Generation
 */
public interface ReportService {
    byte[] getGroceryReport(String itemName) throws IOException;
}
