package com.grocery.assignment.report.service.impl;

import com.grocery.assignment.report.exception.ReportGenerationException;
import com.grocery.assignment.report.service.ReportService;
import com.grocery.assignment.report.vo.GroceryItem;
import lombok.extern.slf4j.Slf4j;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * Service Implmentation
 */
@Service
@Slf4j
public class ReportServiceImpl implements ReportService {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public byte[] getGroceryReport(String itemName) throws IOException {
        log.info("Creating line chart with fetched data");
        return createChart(itemName);
    }

    private byte[] createChart(String itemName) throws IOException {

        JFreeChart chart = ChartFactory.createLineChart(
                "Price Trend Over Time",
                "Date/Time",
                "Price (€)",
                 createDataSet(itemName),
                 PlotOrientation.VERTICAL,
                true,
                true,
                false
        );
        File file = new File("report.jpg");
        ChartUtils.saveChartAsJPEG(file, chart, 1000, 800);
        byte[] fileContent = Files.readAllBytes(file.toPath());
        return fileContent;
    }

    private DefaultCategoryDataset createDataSet(String itemName) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        log.info("Getting data from rest end-point");
        ResponseEntity<List<GroceryItem>> responseEntity = restTemplate.exchange("http://localhost:9002/grocery/getItemByNamesForReport?itemName=" + itemName, HttpMethod.GET, null, new ParameterizedTypeReference<List<GroceryItem>>(){});
        java.util.List<GroceryItem> groceryItemList = responseEntity.getBody();

        for(GroceryItem item : groceryItemList) {
            Double price = item.getItemPrice() == null ? 0 : item.getItemPrice();
            dataset.addValue(price, price, item.getPriceDate());
        }

        return dataset;
    }
}
