package com.grocery.assignment.report.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * data bean
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroceryItem {
    private Double itemPrice;
    private LocalDateTime priceDate;
}
